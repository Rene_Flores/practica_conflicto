<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AlumnoController;

Route::get('/generamos', [AlumnoController::class, 'index'])->name("alumnos.index");
Route::get('/', [AlumnoController::class, 'index'])->name("alumnos.index");
